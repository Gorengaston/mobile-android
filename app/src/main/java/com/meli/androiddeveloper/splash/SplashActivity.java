package com.meli.androiddeveloper.splash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;

import com.meli.androiddeveloper.R;
import com.meli.androiddeveloper.search.SearchActivity;

public class SplashActivity extends AppCompatActivity {
    private static final int PERMISSION_ID = 44;
    private Handler mHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_splash);
        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this,SearchActivity.class);
                startActivity(intent);
                finish();
            }
        },2000);
    }
}

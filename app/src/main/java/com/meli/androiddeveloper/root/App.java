package com.meli.androiddeveloper.root;

import android.app.Application;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.meli.androiddeveloper.network.ApiModule;
import com.meli.androiddeveloper.productslist.ProductListModule;
import com.meli.androiddeveloper.search.SearchModule;

/**
 * Clase que implementa la interfaz {@link AplicationComponent}
 * Se realiza el builder el cual nos permite proveer los módulos que tiene cómo dependencias nuestro Component {@link AplicationModule}
 *
 * @author Gaston Goren
 */
public class App extends Application {

    private AplicationComponent aplicationComponent;
    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    public void onCreate() {
        super.onCreate();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        aplicationComponent = DaggerAplicationComponent.builder()
                .aplicationModule(new AplicationModule(this))
                .searchModule(new SearchModule())
                .apiModule(new ApiModule())
                .productListModule(new ProductListModule())
                .build();
    }

    /**
     *
     * @return objeto aplicationComponent que nos permitirá tener acceso al método inject en las clases deseadas.
     */
    public AplicationComponent getAplicationComponent() {
        return aplicationComponent;
    }
}

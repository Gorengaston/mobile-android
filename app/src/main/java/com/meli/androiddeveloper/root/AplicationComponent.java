package com.meli.androiddeveloper.root;



import com.meli.androiddeveloper.network.ApiModule;
import com.meli.androiddeveloper.productslist.ProductListActivity;
import com.meli.androiddeveloper.productslist.ProductListModule;
import com.meli.androiddeveloper.search.SearchActivity;
import com.meli.androiddeveloper.search.SearchModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Componente Dagger {@link App} .
 * Se declaran los modulos que se implementaran en el component.
 * Intarface donde se crean los metodos utilizados para inyectar los modulos en clases que lo implementen{@link App}
 *
 * @author Gaston Goren
 */

@Singleton
@Component(modules = {AplicationModule.class, SearchModule.class, ApiModule.class, ProductListModule.class})

public interface AplicationComponent {

    void inject(SearchActivity target);
    void inject(ProductListActivity target);

}

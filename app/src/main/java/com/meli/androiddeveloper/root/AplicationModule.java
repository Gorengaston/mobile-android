package com.meli.androiddeveloper.root;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
/**
 * Clase utilizada para vincular nuestra clase de aplicación como contexto en el componente de aplicación {@link App}
 *
 * @author Gaston Goren
 */
@Module
    class AplicationModule {

    private Application application;

   AplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideContext(){
        return application;
    }
}


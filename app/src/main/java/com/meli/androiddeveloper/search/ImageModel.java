package com.meli.androiddeveloper.search;

/**
 * @author Gaston Goren
 */
public class ImageModel {
    private String category;
    private Integer image;

    ImageModel(String category, Integer image) {
        this.category = category;
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }
}

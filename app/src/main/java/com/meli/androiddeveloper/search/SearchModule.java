package com.meli.androiddeveloper.search;

import com.meli.androiddeveloper.network.ApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Modulo Dagger lo utilizamos para declarar las instanacias de las clases {@link SearchPresenter}
 */
@Module
public class SearchModule {
    @Provides
   SearchMVP.Presenter providesSearchPresenter (SearchMVP.Model model){
        return  new SearchPresenter(model);
    }
    @Provides
    SearchMVP.Model providesSearchModel (Repository repository){
        return  new SearchModel(repository);
    }
    @Singleton
    @Provides
    Repository providesSearchRepository (ApiService mApiService){
        return  new SearchRepository(mApiService);
    }
}

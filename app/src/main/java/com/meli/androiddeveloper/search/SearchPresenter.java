package com.meli.androiddeveloper.search;

import android.util.Log;

import com.meli.androiddeveloper.network.models.Results;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Escucha las acciones del usuario desde la interfaz de usuario ({@link SearchActivity}), recupera los datos y actualiza
 * la misma cuando es necesario.
 *
 * @author Gaston Goren
 */

public class SearchPresenter implements SearchMVP.Presenter {

    private final String TAG = "SearchPresenter";
    private SearchMVP.View view;
    private SearchMVP.Model model;
    private Disposable subscription = null;
    private Scheduler suscribe;
    private Scheduler observe;


    @Inject
    SearchMVP.Presenter presenter;

    public SearchPresenter (SearchMVP.Model model){
        this.model = model;
    }

    /**
     * @param search establece el parametro de busqueda para la obtencion de resultados a traves del model {@link SearchModel}.
     * @throws Exception error en la busqueda.
     */
    @Override
    public void loadData(String search) {


        subscription = model.searchResult(search)
                .subscribeOn(suscribe)
                .observeOn(observe)
                .subscribeWith(new DisposableObserver<Results>() {
                    @Override
                    public void onNext(Results results) {
                        if (view != null){
                            if (!results.getTitle().equals("") && results != null){
                                view.foundSearch();
                                view.updateData(results);
                            }else {
                                Log.e(TAG, "No se obtuvieron resultados ");
                                view.showUserError();
                            }

                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (view != null){
                            Log.e(TAG, "Error en la carga " + e.getMessage());
                           view.showUserError();
                        }
                    }
                    @Override
                    public void onComplete() {

                    }
                });


    }

    /**
     * Utilizado para liberar recursos evitando un memory leaks.
     */
    @Override
    public void rxJavaUnsuscribe() {
        if(subscription != null && !subscription.isDisposed()){
            subscription.dispose();
        }
    }
    /**
     * Setea la vista para poder realizar la carga de datos y enviar mensajes al usuario
     */
    @Override
    public void setView(SearchMVP.View view) {
        if(view != null){
            this.view = view;
        }

    }

    @Override
    public void schedulers(Scheduler suscribe, Scheduler observe) {
        this.suscribe = suscribe;
        this.observe = observe;
    }
}

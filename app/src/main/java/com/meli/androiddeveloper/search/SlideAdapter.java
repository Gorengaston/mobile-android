package com.meli.androiddeveloper.search;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.meli.androiddeveloper.R;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Clase que se usa como adpatador de ViewPager ({@link SearchActivity})
 *
 * @author Gaston Goren
 *
 */

public class SlideAdapter extends PagerAdapter {

    private final String TAG = "Slide adapter";
    private Context context;
    /** Seteamos por medio del constructor la lista de ({@link ImageModel}) utilizadas para
     * mostrar las categorias en la pantalla principal
     */
    private List<ImageModel> images;
    private OnClickItemListner onClickItemListner;

    @BindView(R.id.image_view)
    ImageView imageView;
    @BindView(R.id.linearLayout)
    LinearLayout linearLayout;

    SlideAdapter(Context context, List<ImageModel> images, OnClickItemListner onClickItemListner) {
        this.context = context;
        this.images = images;
        this.onClickItemListner = onClickItemListner;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_slider, null);
        ButterKnife.bind(this,view);
        imageView.setImageResource(images.get(position).getImage());
        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);
        imageView.setClickable(true);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"touch image");
                onClickItemListner.selectedCategory(images.get(position).getCategory());
            }
        });
        return view;
    }
    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
    public interface OnClickItemListner{
        void selectedCategory(String category);
    }
}

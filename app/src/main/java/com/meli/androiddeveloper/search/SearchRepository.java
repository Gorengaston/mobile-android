package com.meli.androiddeveloper.search;


import com.meli.androiddeveloper.network.ApiService;
import com.meli.androiddeveloper.network.models.ProductSearch;
import com.meli.androiddeveloper.network.models.Results;


import io.reactivex.Observable;
import io.reactivex.ObservableSource;

import io.reactivex.functions.Function;

/**
 * Implementación  para obtener resultados de la busqueda  en un servidor remoto.
 *
 * @author  Gaston Goren
 */
public class SearchRepository implements Repository {
    private ApiService mApiService;


   SearchRepository(ApiService mApiService){
        this.mApiService = mApiService;

    }

    /**
     * @param search  producto a buscar
     * @return resultado de la busqueda de tipo {@link Results} notifica cambios en el resultado se utiliza {@code Observable}
     */
    @Override
    public Observable<Results> getResult(String search) {

        Observable<ProductSearch> resultsObservable = mApiService.getSearch(search);

        return resultsObservable.concatMap(new Function<ProductSearch, ObservableSource<? extends Results>>() {
            @Override
            public ObservableSource<? extends Results> apply(ProductSearch productSearch) {
                return Observable.fromIterable(productSearch.getResults());
            }
        });

    }
}

package com.meli.androiddeveloper.search;

import com.meli.androiddeveloper.network.models.Results;

import io.reactivex.Observable;

/**
 * Clase utilizada para realizar peticiones al repositorio cuando el presentador lo requiera.
 *
 * @author Gaston Goren
 */
public class SearchModel implements SearchMVP.Model {

    private Repository repository;

    SearchModel(Repository repository){

        this.repository = repository;
    }

    /**
     * @param search parametro de busqueda en el repositorio
     * @return {@code Observable}  de tipo Result el cual es utilizado por el presentador para cargar
     * los datos en la vista.
     */
    @Override
    public Observable <Results> searchResult(String search) {
        return repository.getResult(search);
    }
}

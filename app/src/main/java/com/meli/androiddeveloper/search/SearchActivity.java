package com.meli.androiddeveloper.search;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.meli.androiddeveloper.R;
import com.meli.androiddeveloper.network.models.Results;
import com.meli.androiddeveloper.productslist.ProductListActivity;
import com.meli.androiddeveloper.root.App;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Interfaz de usuario, muestra los resultados obtenidos por el buscador.
 *
 * @author Gaston Goren
 */

public class  SearchActivity extends AppCompatActivity implements SearchMVP.View,SearchView.OnQueryTextListener{


    @Inject
    SearchMVP.Presenter presenter;

    //Variable que define la cantidad minima de caracteres a ingresar antes de comenzar la busqueda
    private static final int SEARCH_QUERY_THRESHOLD =3 ;
    private static final int PERMISSION_ID = 44;
    private final String TAG = "Search Activity";

    private List<Results> result = new ArrayList<>();
    private Context context;
    private SearchView searchView;
    private List<ImageModel> listImages = new ArrayList<>();


    @BindView(R.id.view_pager_search)
    ViewPager viewPager;
    @BindView(R.id.tab_layout_view_pager_search)
    TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        context = SearchActivity.this;
        validatePermission();
        ((App) getApplication()).getAplicationComponent().inject(this);
        ButterKnife.bind(this);
        viewPager();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setView(this);
        presenter.schedulers(Schedulers.io(), AndroidSchedulers.mainThread());
    }


    @Override
    protected void onStop() {
        super.onStop();
        presenter.rxJavaUnsuscribe();
        result.clear();
    }



    public boolean onCreateOptionsMenu(Menu menu) {
        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        //Search menu
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem menuItem =  menu.findItem(R.id.search);
        searchView = (SearchView) menuItem.getActionView();
        if (searchManager != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("Encuentra lo que buscas");
        searchView.setSuggestionsAdapter(new SimpleCursorAdapter(
                context,R.layout.search_view_suggestion_row, null,
                new String[] {SearchManager.SUGGEST_COLUMN_TEXT_1 },
                new int[] { R.id.text }));
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionClick(int position) {

                return onSuggestionSelect(position);
            }

            @Override
            public boolean onSuggestionSelect(int position) {
                Cursor cursor = (Cursor) searchView.getSuggestionsAdapter().getItem(position);
                String product = cursor.getString(cursor.getColumnIndex(SearchManager.SUGGEST_COLUMN_TEXT_1));
                cursor.close();
                Intent intent = new Intent(SearchActivity.this, ProductListActivity.class);
                intent.putExtra("type","product");
                intent.putExtra("product" , product);
                startActivity(intent);
                return true;
            }

        });

        return true;
    }


    /**
     * Se utiliza para la carga de los datos obtenidos y mostrarlos en la vista.
     * @param results devuelve el resultado obtenido de la busqueda.
     */
    @Override
    public void updateData(Results results) {
        this.result.add(results);
        cursorAdapter();

    }

    @Override
    public void foundSearch() {
        Log.d(TAG,"Se cargo la busqueda");

    }

    /**
     * Metodo utilizado para infromar al usuario errores que pueden surgir en la busqueda de datos
     */
    @Override
    public void showUserError() {
        Toast.makeText(this,"Error en la busqurda",Toast.LENGTH_SHORT).show();
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        if (query.length() >= SEARCH_QUERY_THRESHOLD) {
            searchView.getSuggestionsAdapter().changeCursor(null);
            return true;
        }else {
            return false;
        }
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.length() >= SEARCH_QUERY_THRESHOLD) {
            presenter.loadData(newText);
            result.clear();
        } else {
            searchView.getSuggestionsAdapter().changeCursor(null);
        }
        return true;
    }
    //Metodo que crea crea el cursor necesario para mostrar las sugerencias del search view
    private void cursorAdapter (){

        String[] sAutocompleteColNames = new String[] {
                SearchManager.SUGGEST_COLUMN_ICON_1,
                BaseColumns._ID,
                SearchManager.SUGGEST_COLUMN_TEXT_1,
                SearchManager.SUGGEST_COLUMN_TEXT_2
        };

        MatrixCursor cursor = new MatrixCursor(sAutocompleteColNames);

            for (int index = 0; index < result.size(); index++){
                Object[] row = new Object[] {R.drawable.ic_search_black_24dp, index , result.get(index).getTitle(), result.get(index).getCategoryId() };
                cursor.addRow(row);
        }

        searchView.getSuggestionsAdapter().changeCursor(cursor);

    }


    private void populateImage(){
        listImages.add(new ImageModel("MLA1744",R.drawable.car_category));
        listImages.add(new ImageModel("MLA1055",R.drawable.celular_category));
        listImages.add(new ImageModel("MLA109042",R.drawable.clothing_category));
    }

    private class SliderTimer extends TimerTask {

        @Override
        public void run() {
            SearchActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPager.getCurrentItem() < listImages.size() - 1) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    } else {
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }
    public void viewPager(){
        populateImage();
        viewPager.setAdapter(new SlideAdapter(this, listImages, new SlideAdapter.OnClickItemListner() {
            @Override
            public void selectedCategory(String category) {
                Intent intent = new Intent(SearchActivity.this, ProductListActivity.class);
                intent.putExtra("type","category");
                intent.putExtra("category" , category);
                startActivity(intent);
            }
        }));
        tabLayout.setupWithViewPager(viewPager, true);
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(), 2000, 6000);

    }
    public  void  validatePermission(){
        if (checkPermissions()){

        }else {
            requestPermissions();
        }
    }
    boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }
    void requestPermissions() {
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

}

package com.meli.androiddeveloper.search;

import com.meli.androiddeveloper.network.models.Results;

import io.reactivex.Observable;

public interface Repository {
    Observable<Results> getResult(String search);
}

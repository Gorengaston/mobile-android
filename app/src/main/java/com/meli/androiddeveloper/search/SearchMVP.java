package com.meli.androiddeveloper.search;




import com.meli.androiddeveloper.network.models.Results;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * Especifica el contrato entre vista,modelo y presentador.
 *
 * @author Gaston Goren
 */
public interface SearchMVP {

    interface View {
        void updateData(Results results);
        void showUserError();
        void foundSearch();
    }

    interface Presenter{
        void loadData(String search);
        void rxJavaUnsuscribe();
        void setView(SearchMVP.View view);
        void schedulers(Scheduler suscribe, Scheduler observe);
    }

    interface Model{
        Observable<Results> searchResult(String search);
    }
}

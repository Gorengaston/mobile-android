package com.meli.androiddeveloper.network;

import com.google.gson.GsonBuilder;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Modulo de dagger
 * Realiza el builder de Retrofit y se instancia el mismo.
 *
 * @author Gaston Goren
 */
@Module
public class ApiModule {
    private static Retrofit retrofit = null;
    private static String BASE_URL = "https://api.mercadolibre.com/sites/MLA/";
    @Provides
    public static Retrofit getClient(String baseUrl) {
        if(retrofit == null) {
            GsonBuilder builder = new GsonBuilder();
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create(builder.create()))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return retrofit;
    }
    @Provides
    public static ApiService provideApiService() {
        return getClient(BASE_URL).create(ApiService.class);
    }


}


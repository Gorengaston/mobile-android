package com.meli.androiddeveloper.network;

import com.meli.androiddeveloper.network.models.ProductSearch;
import com.meli.androiddeveloper.network.models.Results;


import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Se declaran los metodos para realizar pedidos al servidor remoto.
 *
 * @author gaston
 * */

public interface ApiService {
    @GET("search")
    Observable<ProductSearch> getSearch (@Query("q") String search);

    @GET("search")
    Observable<ProductSearch> getCategory (@Query("category") String search);


}

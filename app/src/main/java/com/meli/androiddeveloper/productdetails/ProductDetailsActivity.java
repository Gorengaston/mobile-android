package com.meli.androiddeveloper.productdetails;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.meli.androiddeveloper.R;
import com.meli.androiddeveloper.network.models.Results;
import com.meli.androiddeveloper.productslist.ProductListActivity;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author gaston
 * @version 1.0.0
 * Clase que muestra la informacion del producto seleccionado
 *
 */
public class ProductDetailsActivity extends AppCompatActivity {

    private final String TAG = "ProductDetailsActivity";
    @BindView(R.id.text_send_address)
    TextView textViewSendAddres;
    @BindView(R.id.image_view_photo_product)
    ImageView imageViewPhoto;
    @BindView(R.id.text_view_condition)
    TextView textViewCondition;
    @BindView(R.id.text_view_title_product)
    TextView textViewTitleProduct;
    @BindView(R.id.text_view_title_price)
    TextView textViewTitlePrice;
    @BindView(R.id.text_view_product_color)
    TextView textViewColor;
    @BindView(R.id.text_view_product_quantity)
    TextView textViewTitleQuantity;
    @BindView(R.id.button_buy)
    Button buttonBuy;
    @BindView(R.id.button_add_product)
    Button buttonAddProduct;
    @BindView(R.id.text_view_address)
    TextView textViewAddress;
    @BindView(R.id.text_view_mercado_status)
    TextView textViewSellerStatus;

    Results results;

    FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        ButterKnife.bind(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        checkPermissions();
        populateView();

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent;
        if (item.getItemId() == android.R.id.home) {
            intent = new Intent(ProductDetailsActivity.this, ProductListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }
        return true;
    }

    /**
     * Metodo que setea valores a las propiedades  que contiene la clase
     */
    private void populateView() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            results = (Results) extras.getSerializable("result");//Obtaining data
        }
        //Photo
        Glide.with(this)
                .load(results.getThumbnail())
                .apply(new RequestOptions()
                        .fitCenter()
                        .format(DecodeFormat.PREFER_ARGB_8888)
                        .override(Target.SIZE_ORIGINAL))
                .into(imageViewPhoto);


        //Condition
        if ("new".equals(results.getCondition())) {
            textViewCondition.setText(String.format("%s", "Nuevo"));
        } else {
            textViewCondition.setText(String.format("%s", "Usado"));
        }
        //Price
        textViewTitlePrice.setText(getDoubleFormat(results.getPrice()));
        //Title
        textViewTitleProduct.setText(results.getTitle());
        //Quantity
        textViewTitleQuantity.setText(String.format("Cantidad %s", results.getSoldQuantity()));
        String.format("Cantidad", results.getSoldQuantity());
        //Address
        textViewAddress.setText(results.getAddress().getStateName() + " " + results.getAddress().getCityName());
        //Seller status
        if (results.getSeller().getPowerSellerStatus() != null) {
            textViewSellerStatus.setText((String) results.getSeller().getPowerSellerStatus());
        } else {
            textViewSellerStatus.setText(String.format("%s", "No tiene categoria todavia"));

        }


    }
    String getDoubleFormat(double value) {
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.GERMAN);
        nf.setGroupingUsed(true);
        return "$ " + nf.format(value);
    }

    boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            return true;
        }
        return false;
    }
        private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }


    private void getLastLocation(){
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {
                                    geoDecoder(location.getLatitude(),location.getLongitude());
                                }
                            }
                        }
                );
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }

    }
    private void requestNewLocationData(){

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }
    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            geoDecoder(mLastLocation.getLatitude(),mLastLocation.getLongitude());

        }
    };
    private void geoDecoder(double latitude, double longitude){
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            textViewSendAddres.setVisibility(View.VISIBLE);
            textViewSendAddres.setText(addresses.get(0).getAddressLine(0));
        } catch (IOException e) {
            Log.e(TAG,"No se pudo obtener el geolocalizacion");
            e.printStackTrace();
        }
        // Only if available else return NULL
    }

}

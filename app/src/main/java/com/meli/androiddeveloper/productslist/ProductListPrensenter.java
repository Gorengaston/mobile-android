package com.meli.androiddeveloper.productslist;

import android.util.Log;

import com.meli.androiddeveloper.network.models.Results;
import com.meli.androiddeveloper.search.SearchActivity;
import com.meli.androiddeveloper.search.SearchModel;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Escucha las acciones del usuario desde la interfaz de usuario ({@link ProductListActivity}), recupera los datos y actualiza
 * la misma cuando es necesario.
 *
 * @author Gaston Goren
 */

public class ProductListPrensenter implements ProductsListMVP.Presenter {
    private final String TAG = "ProductListPrensenter";
    private ProductsListMVP.Model model;
    private ProductsListMVP.View view;
    private Disposable subscription = null;
    private Scheduler suscribe;
    private Scheduler observe;

    @Inject
    ProductsListMVP.Presenter presenter;

    public ProductListPrensenter(ProductsListMVP.Model model){
        this.model = model;
    }
    /**
     *
     * @param product establece el parametro de busqueda para la obtencion de resultados a traves del model {@link ProducListModel}.
     * @throws Exception error en la busqueda.
     */
    @Override
    public void loadSearch(String product) {
        Log.d(TAG,product);
        subscription = model.searchResult(product)
                .subscribeOn(suscribe)
                .observeOn(observe)
                .subscribeWith(new DisposableObserver<Results>() {
                    @Override
                    public void onNext(Results results) {
                        if (!results.getTitle().equals("") && results != null){
                            view.foundSearch();
                            view.updateData(results);
                        }else {
                            Log.e(TAG, "No se obtuvieron resultados ");
                            view.showUserError();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null){

                            view.showUserError();
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void loadCategory(String category) {
        subscription = model.searchCategory(category)
                .subscribeOn(suscribe)
                .observeOn(observe)
                .subscribeWith(new DisposableObserver<Results>() {
                    @Override
                    public void onNext(Results results) {
                        if (!results.getTitle().equals("") && results != null){
                            view.foundSearch();
                            view.updateData(results);
                        }else {
                            Log.e(TAG, "No se obtuvieron resultados ");
                            view.showUserError();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null){
                            Log.e(TAG,e.getMessage());
                            view.showUserError();
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }
    /**
     * Utilizado para liberar recursos evitando un memory leaks.
     */
    @Override
    public void rxJavaUnsuscribe() {
        if(subscription != null && !subscription.isDisposed()){
            subscription.dispose();
        }
    }
    /**
     * Utilizado para liberar recursos evitando un memory leaks.
     */
    @Override
    public void setView(ProductsListMVP.View view) {
        this.view = view;
    }

    @Override
    public void schedulers(Scheduler suscribe, Scheduler observe) {
        this.suscribe = suscribe;
        this.observe = observe;

    }

}

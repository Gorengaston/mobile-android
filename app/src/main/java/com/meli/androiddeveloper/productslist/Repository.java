package com.meli.androiddeveloper.productslist;

import com.meli.androiddeveloper.network.models.Results;

import io.reactivex.Observable;


public interface Repository {
    Observable<Results> getResult(String product);
    Observable<Results> getCategory(String category);
}

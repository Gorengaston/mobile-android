package com.meli.androiddeveloper.productslist;

import com.meli.androiddeveloper.network.ApiService;
import com.meli.androiddeveloper.search.SearchPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
/**
 * Modulo Dagger lo utilizamos para declarar las instanacias de las clases {@link ProductListPrensenter}
 */

@Module
public class ProductListModule {
    @Provides
    ProductsListMVP.Presenter providesProductListPresnter (ProductsListMVP.Model model){
        return  new ProductListPrensenter(model);
    }

    @Provides
    ProductsListMVP.Model providesProductListModel (Repository repository){
        return  new ProducListModel(repository);
    }
    @Singleton
    @Provides
    Repository providesRepository (ApiService apiService){
        return new ProductListRepository(apiService);
    }
}

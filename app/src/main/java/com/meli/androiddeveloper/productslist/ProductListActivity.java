package com.meli.androiddeveloper.productslist;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.meli.androiddeveloper.R;
import com.meli.androiddeveloper.network.models.Results;
import com.meli.androiddeveloper.productdetails.ProductDetailsActivity;
import com.meli.androiddeveloper.root.App;
import com.meli.androiddeveloper.search.SearchActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Interfaz de usuario, muestra los resultados obtenidos por el buscador.
 *
 * @author gaston
 */

public class ProductListActivity extends AppCompatActivity implements ProductsListMVP.View {
    private final String TAG = "ProductListActivity";
    @Inject
    ProductsListMVP.Presenter presenter;
    @BindView(R.id.constraint_layout_product_list)
    ConstraintLayout constraintLayout;
    @BindView(R.id.recycler_view_product_list)
    RecyclerView recyclerView;
    private String bundle;
    private String type;
    Parcelable savedRecyclerLayoutState;

    private ListAdapter listAdapter;
    private List<Results> resultList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        ButterKnife.bind(this);
        ((App) getApplication()).getAplicationComponent().inject(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //PRODUCT LIST
        listAdapter = new ListAdapter(resultList, new ListAdapter.OnClickItemListner() {
            @Override
            public void onClickSelectedProduct(Results results) {
                Intent intent = new Intent(ProductListActivity.this, ProductDetailsActivity.class);
                intent.putExtra("result",results);
                startActivity(intent);

            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setAdapter(listAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);


    }
    @Override
    public void updateData(Results results) {
            resultList.add(results);
            listAdapter.notifyItemInserted(resultList.size() - 1);
            listAdapter.notifyDataSetChanged();
            Objects.requireNonNull(recyclerView.getLayoutManager()).onRestoreInstanceState(savedRecyclerLayoutState);
    }

    @Override
    public void showUserError() {
        Toast.makeText(this,"Error al cargar la busqueda",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void foundSearch() {
        Log.d(TAG,"La carga fue exitosa");
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
           case android.R.id.home:
               intent = new Intent(ProductListActivity.this, SearchActivity.class);
               intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
               startActivity(intent);
       }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setView(this);
        presenter.schedulers(Schedulers.io(), AndroidSchedulers.mainThread());

        //Switch segun el tipo de busqueda que se quiera realizar, puede ser por produto o categoria
        type = getIntent().getStringExtra("type");
        if (type != null) {
            switch (type){
                case "product":
                    bundle = getIntent().getStringExtra("product");
                    Log.d(TAG,bundle);
                    presenter.loadSearch(bundle);
                    break;
                case "category":
                    bundle = getIntent().getStringExtra("category");
                    presenter.loadCategory(bundle);
                    break;
            }
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.rxJavaUnsuscribe();
        resultList.clear();
        listAdapter.notifyDataSetChanged();

    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("lastPosition", Objects.requireNonNull(recyclerView.getLayoutManager()).onSaveInstanceState());
        outState.putString("bundle",bundle);
        outState.putString("type",type);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        savedRecyclerLayoutState = savedInstanceState.getParcelable("lastPosition");
        Objects.requireNonNull(recyclerView.getLayoutManager()).onRestoreInstanceState(savedRecyclerLayoutState);
        bundle = savedInstanceState.getString("bundle");
        type = savedInstanceState.getString("type");
    }

}

package com.meli.androiddeveloper.productslist;

import com.meli.androiddeveloper.network.models.Results;
import io.reactivex.Observable;
import io.reactivex.Scheduler;

/**
 * Especifica el contrato entre vista,modelo y presentador.
 *
 * @author Gaston Goren
 */

public interface ProductsListMVP {
    interface View {
        void updateData(Results results);
        void showUserError();
        void foundSearch();
    }

    interface Presenter{
        void loadSearch(String search);
        void loadCategory(String category);
        void rxJavaUnsuscribe();
        void setView(ProductsListMVP.View view);
        void schedulers(Scheduler suscribe, Scheduler observe);
    }

    interface Model{
        Observable<Results> searchResult(String product);
        Observable<Results> searchCategory(String category);
    }

}

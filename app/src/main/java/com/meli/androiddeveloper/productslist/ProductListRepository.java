package com.meli.androiddeveloper.productslist;

import com.meli.androiddeveloper.network.ApiService;
import com.meli.androiddeveloper.network.models.ProductSearch;
import com.meli.androiddeveloper.network.models.Results;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
/**
 * Implementación  para obtener resultados de la busqueda  en un servidor remoto.
 *
 * @author  Gaston Goren
 */

public class ProductListRepository implements Repository {
    private ApiService apiService;

    ProductListRepository(ApiService apiService){
        this.apiService = apiService;
    }

    /**
     * @param product establece el tipo de producto a buscar
     * @return resultado de la busqueda de tipo {@link Results} notifica cambios en el resultado se utiliza {@code Observable}
     */
    @Override
    public Observable<Results> getResult(String product) {
        Observable<ProductSearch> productSearchObservable = apiService.getSearch(product);

        return productSearchObservable.concatMap(new Function<ProductSearch, Observable<Results>>() {
            @Override
            public Observable<Results> apply(ProductSearch productSearch) {
                return Observable.fromIterable(productSearch.getResults());
            }
        });
    }
    /**
     * @param category busca productos por categoria
     * @return resultado de la busqueda de tipo {@link Results} notifica cambios en el resultado se utiliza {@code Observable}
     */
    @Override
    public Observable<Results> getCategory(String category) {
        Observable<ProductSearch> productSearchObservable = apiService.getCategory(category);
        return productSearchObservable.concatMap(new Function<ProductSearch, Observable<Results>>() {
            @Override
            public Observable<Results> apply(ProductSearch productSearch)  {
                return Observable.fromIterable(productSearch.getResults());
            }
        });
    }

}

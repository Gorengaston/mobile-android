package com.meli.androiddeveloper.productslist;

import com.meli.androiddeveloper.network.models.Results;

import io.reactivex.Observable;

/**
 * Implementa los metodos necesarios para realizar una peticion al servidor y devolver los resulstados
 * al presentador
 */
public class ProducListModel implements ProductsListMVP.Model {
    Repository repository;

    public ProducListModel(Repository repository){

        this.repository = repository;
    }

    @Override
    public Observable<Results> searchResult(String product) {
        return repository.getResult(product);
    }

    @Override
    public Observable<Results> searchCategory(String category) {
        return repository.getCategory(category);
    }

}

package com.meli.androiddeveloper.productslist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.meli.androiddeveloper.R;
import com.meli.androiddeveloper.network.models.Results;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Adaptador utilizado para mostrar la lista de resultaos en el RecyclerView {@link ProductListActivity}
 *
 * @author Gaston Goren
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.VieHolders> {

    private List<Results> result;
    private View view;
    private OnClickItemListner onClickItemListner;


   ListAdapter(List<Results> results, OnClickItemListner onClickItemListner){
        result = new ArrayList<>();
        this.result = results;
        this.onClickItemListner = onClickItemListner;
    }

    @NonNull
    @Override
    public VieHolders onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_adapter,parent,false);
        return new ListAdapter.VieHolders(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VieHolders holder, int position) {
        Results results = result.get(position);

        holder.Bind(results, onClickItemListner);
        Glide.with(view)
                .load(results.getThumbnail())
                .into(holder.imageView);

        holder.textViewTitle.setText(results.getTitle());
        holder.textViewPrice.setText(getDoubleFormat(results.getPrice()));

    }
    String getDoubleFormat(double value) {
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.GERMAN);
        nf.setGroupingUsed(true);
        return "$ " + nf.format(value);
    }

    @Override
    public int getItemCount() {
        return result == null ? 0 : result.size();
    }

    class VieHolders extends RecyclerView.ViewHolder {
        @BindView(R.id.image_view_photo_product)
        ImageView imageView;
        @BindView(R.id.text_view_title_product)
        TextView textViewTitle;
        @BindView(R.id.text_view_price_product)
        TextView textViewPrice;

       VieHolders(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }
    void Bind (final Results result, final OnClickItemListner onClickItemListner){

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickItemListner.onClickSelectedProduct(result);
                }
            });
    }

    }
    public interface  OnClickItemListner {
        void onClickSelectedProduct(Results results);
    }
}

package com.meli.androiddeveloper;

import com.meli.androiddeveloper.network.models.Results;
import com.meli.androiddeveloper.search.SearchMVP;

import org.junit.Before;
import org.junit.Test;



import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SearchPresenter {
    private com.meli.androiddeveloper.search.SearchPresenter presenter;
    private SearchMVP.Model mockModel;
    private SearchMVP.View mockView;


    @Before
    public void init(){


        mockModel = mock(SearchMVP.Model.class);
        mockView = mock(SearchMVP.View.class);

        presenter = new com.meli.androiddeveloper.search.SearchPresenter(mockModel);
        presenter.setView(mockView);
        presenter.schedulers(Schedulers.trampoline(),Schedulers.trampoline());

    }

    @Test
    public void loadDataWhenSearchIsValid(){

        String query ="prueba";
        Results results = new Results("titulo prueba");

        Observable observable = Observable.just(results);
        when(mockModel.searchResult(query)).thenReturn(observable);

        presenter.loadData(query);
        verify(mockView, times(1)).foundSearch();
        presenter.rxJavaUnsuscribe();

    }
    @Test
    public void loadDataWhenSearchIsEmpty(){

        String query ="";
        Results results = new Results("");

        Observable observable = Observable.just(results);
        when(mockModel.searchResult(query)).thenReturn(observable);

        presenter.loadData(query);
        verify(mockView, times(1)).showUserError();
        presenter.rxJavaUnsuscribe();

    }

}

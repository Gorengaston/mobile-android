package com.meli.androiddeveloper;

import com.meli.androiddeveloper.network.models.Results;
import com.meli.androiddeveloper.productslist.ProductListPrensenter;
import com.meli.androiddeveloper.productslist.ProductsListMVP;

import org.junit.Before;
import org.junit.Test;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Gaston Goren.
 */
public class ProductListPresenter {
    private ProductListPrensenter presenter;
    private ProductsListMVP.Model mockModel;
    private ProductsListMVP.View mockView;


    @Before
    public void init(){
        mockModel = mock(ProductsListMVP.Model.class);
        mockView = mock(ProductsListMVP.View.class);

        presenter = new ProductListPrensenter(mockModel);
        presenter.setView(mockView);
        presenter.schedulers(Schedulers.trampoline(),Schedulers.trampoline());

    }

    @Test
    public void loadProductWhenSearchIsValid(){

        String query ="prueba";
        Results results = new Results("titulo prueba");

        Observable observable = Observable.just(results);
        when(mockModel.searchResult(query)).thenReturn(observable);

        presenter.loadSearch(query);
        verify(mockView, times(1)).foundSearch();
        presenter.rxJavaUnsuscribe();

    }
    @Test
    public void loadProductWhenSearchIsEmpty(){

        String query ="";
        Results results = new Results("");

        Observable observable = Observable.just(results);
        when(mockModel.searchResult(query)).thenReturn(observable);

        presenter.loadSearch(query);
        verify(mockView, times(1)).showUserError();
        presenter.rxJavaUnsuscribe();

    }
    @Test
    public void loadCategoryWhenSearchIsValid(){

        String query ="";
        Results results = new Results("titulo prueba");

        Observable observable = Observable.just(results);
        when(mockModel.searchResult(query)).thenReturn(observable);

        presenter.loadSearch(query);
        verify(mockView, times(1)).foundSearch();
        presenter.rxJavaUnsuscribe();

    }
    @Test
    public void loadCategoryWhenSearchIsEmpty(){

        String query ="";
        Results results = new Results("");

        Observable observable = Observable.just(results);
        when(mockModel.searchResult(query)).thenReturn(observable);

        presenter.loadSearch(query);
        verify(mockView, times(1)).showUserError();
        presenter.rxJavaUnsuscribe();

    }
}
